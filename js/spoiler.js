(function($) {
    $(function () {
        $('body').on('click', '.spoiler-title', function () {
            $(this).closest('.spoiler').toggleClass('spoiler-open');
        });
    });
})(jQuery);
