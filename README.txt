INTRODUCTION
------------
This module enables the Spoiler plugin from CKEditor.com in your WYSIWYG.

INSTALLATION
------------
1. Make sure you have asset-packagist setup 
2. Enable CKEditor Spoiler in the Drupal admin.
3. Configure your WYSIWYG toolbar to include the buttons.

REQUIREMENTS
------------
CKEditor Module (Core)
Oomphinc Composer Installers Extender
